﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebAPI
{
    public interface IResponse
    {
        bool Success { get; }
        List<string> Errors { get; }
        dynamic Data { get; }

        //string Message { get; set; }
        //bool DidError { get; set; }
        //string ErrorMessage { get; set; }
        HttpStatusCode Status { get; }
    }

    public class MyResponse : IResponse
    {
        public MyResponse()
        {
            this.Errors = new List<string>();
        }

        [JsonProperty("Errors")]
        public List<string> Errors { get; set; }

        [JsonProperty("Data")]
        public dynamic Data { get; set; }

        public HttpStatusCode Status { get; set; }

        [JsonProperty("Success")]
        public bool Success
        {
            get
            {
                return this.Errors.Count == 0;
            }
        }
    }


    public static class ResponseExtensions
    {
        public static IActionResult ToHttpResponse(this IResponse response)
        {
            var status = response.Status == 0 ? (int)HttpStatusCode.OK : (int)response.Status;

            //if (response.Success)
            //    status = (int)HttpStatusCode.OK;

            //else if (!response.Success && response.Status == 0)
            //    status = (int)HttpStatusCode.PreconditionFailed;

            return new ObjectResult(response)
            {
                StatusCode = status
            };
        }

        

       
    }
}
