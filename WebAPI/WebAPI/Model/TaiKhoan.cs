﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Model
{
    public partial class TaiKhoan
    {
        public TaiKhoan()
        {
        }
        public long TaiKhoanId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
