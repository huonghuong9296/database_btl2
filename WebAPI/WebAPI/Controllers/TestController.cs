﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Model;

namespace WebAPI.Controllers
{
    [Route("DonViTinh")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly DataContext _dbContext;

        public TestController(DataContext dbContext) : base()
        {
            this._dbContext = dbContext;
        }
        // GET api/values

        

        [HttpGet]
        public async Task<IActionResult> FindOne(long id)
        {
            var response = new MyResponse();
            try
            {
                
                response.Data = await this._dbContext.TaiKhoan.FirstOrDefaultAsync(d => d.TaiKhoanId == id);

            }
            catch (Exception ex)
            {

            }
            return response.ToHttpResponse();
        }

        [HttpPost]
        public async Task<IActionResult> dangnhap(string username,string password)
        {
            var response = new MyResponse();
            try
            {
                var taiKhoan = new TaiKhoan();
                taiKhoan.Username = username;
                taiKhoan.Password = password;
                this._dbContext.TaiKhoan.Add(taiKhoan);
                await this._dbContext.SaveChangesAsync();
                response.Data = new
                {
                    taiKhoan.Username,
                    taiKhoan.Password,
                };

            }
            catch (Exception ex)
            {

            }
            return response.ToHttpResponse();
        }


    }
}
