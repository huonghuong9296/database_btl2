﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using System;
using WebAPI.Model;

public class DataContext : DbContext
{
	public DataContext(DbContextOptions<DataContext> options): base(options)
	{
	}
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TaiKhoan>();
    }
    public virtual DbSet<TaiKhoan> TaiKhoan { get; set; }
}
